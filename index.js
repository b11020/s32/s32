// require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// creates server
const app = express();
const port = 4000;

// connect to monggoDB
mongoose.connect("mongodb+srv://admin:admin@cluster0.2cnfh.mongodb.net/course-booking-app?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
// allow all resources to access our backend app
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// listening to port
// this syntax will allow flexibility when using the application
// locally or as a hosted application
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT ||
	 port}`);
})